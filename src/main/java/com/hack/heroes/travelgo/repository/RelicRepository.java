package com.hack.heroes.travelgo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hack.heroes.travelgo.entity.Relic;

@Repository
public interface RelicRepository extends CrudRepository<Relic, Long>{

	@Query(value = "SELECT * FROM relic WHERE latitude BETWEEN :minlatitude AND :maxlatitude AND longitude BETWEEN :minlongitude AND :maxlongitude", nativeQuery = true)
	List<Relic> relicsInRadius(@Param("minlatitude")double minlatitude, @Param("maxlatitude")double maxlatitude, @Param("minlongitude")double minlongitude, @Param("maxlongitude")double maxlongitude);
	@Query(value = "SELECT * FROM relic", nativeQuery = true)
	List<Relic> relics();
	@Query(value = "SELECT id FROM relic ORDER BY id DESC LIMIT 1", nativeQuery = true)
	int lastRelicId();
}
