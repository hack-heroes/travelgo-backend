package com.hack.heroes.travelgo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hack.heroes.travelgo.entity.Commune;

@Repository
public interface CommuneRepository extends CrudRepository<Commune, Integer>{

	@Query(value = "SELECT * FROM commune", nativeQuery = true)
	List<Commune> getAllCommunes();
	
}
