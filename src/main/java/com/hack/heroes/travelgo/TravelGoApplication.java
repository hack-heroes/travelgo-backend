package com.hack.heroes.travelgo;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.hack.heroes.travelgo.utils.MathUtils;

@SpringBootApplication
public class TravelGoApplication extends SpringBootServletInitializer{

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public MathUtils mathUtils() {
		return new MathUtils();
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(TravelGoApplication.class);
	}
	
	
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
	    final CorsConfiguration configuration = new CorsConfiguration();
	    final String[] star = {"*"};
	    final String[] methods = {"HEAD", "GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"};
	    final String[] headers = {"X-Auth-Token","Authorization","Access-Control-Allow-Origin","Access-Control-Allow-Credentials"}; 
	    configuration.setAllowedOrigins(Arrays.asList(star));
	    configuration.setAllowedMethods(Arrays.asList(methods));
	    configuration.setAllowCredentials(true);
	    configuration.setAllowedHeaders(Arrays.asList(star));
	    configuration.setExposedHeaders(Arrays.asList(headers));
	    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    source.registerCorsConfiguration("/**", configuration);
	    return source;
	}
	
	public static void main(String[] args) {
		SpringApplication.run(TravelGoApplication.class, args);
	}
}
