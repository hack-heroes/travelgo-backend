package com.hack.heroes.travelgo.maps;

public class CustomRelicUserMap {

	private String username;
	private String identification;
	private String registerNumber;
	private String datingOfObj;
	private double latitude;
	private double longitude;
	
	public CustomRelicUserMap() {
		
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getIdentification() {
		return identification;
	}
	public void setIdentification(String identification) {
		this.identification = identification;
	}
	public String getRegisterNumber() {
		return registerNumber;
	}
	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}
	public String getDatingOfObj() {
		return datingOfObj;
	}
	public void setDatingOfObj(String datingOfObj) {
		this.datingOfObj = datingOfObj;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
}
