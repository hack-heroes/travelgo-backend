package com.hack.heroes.travelgo.maps;

public class RelicUserMap {

	private String username;
	private long relicId;
	
	public RelicUserMap() {
		
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public long getRelicId() {
		return relicId;
	}
	public void setRelicId(long relicId) {
		this.relicId = relicId;
	}
	
}
