package com.hack.heroes.travelgo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hack.heroes.travelgo.entity.Relic;
import com.hack.heroes.travelgo.entity.User;
import com.hack.heroes.travelgo.maps.RelicUserMap;
import com.hack.heroes.travelgo.service.UserService;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(path = "/user")
public class UserController {

	@Autowired 
	private UserService userServices;
	
	@PostMapping(path = "/register")
	public ResponseEntity<String> addNewUser(@RequestBody User user) {
		return userServices.addNewUser(user);
	}
	
	@GetMapping(path = "/{username}")
	public ResponseEntity<User> getUser(@PathVariable String username){
		return userServices.getUser(username);
	}
	
	@GetMapping(path = "/{username}/relics")
	public ResponseEntity<List<Relic>> getUserRelic(@PathVariable String username){
		return userServices.getUserRelic(username);
	}
	
	@GetMapping(path = "/{username}/customs")
	public ResponseEntity<List<Relic>> getUserCustomRelic(@PathVariable String username){
		return userServices.getUserCustomRelic(username);
	}
	
	@GetMapping(path = "/all")
	public ResponseEntity<Iterable<User>> getUsers(){
		return userServices.getUsers();
	}
	
	@PutMapping(path = "/{username}/addexperience/{experience}")
	public ResponseEntity<String> addExperience(@PathVariable String username, @PathVariable int experience){
		return userServices.addExp(username, experience);
	}
	
	@PutMapping(path = "/visit/relic")
	public ResponseEntity<User> visitRelic(@RequestBody RelicUserMap rum){
		return userServices.visitRelic(rum);
	}
	
}
