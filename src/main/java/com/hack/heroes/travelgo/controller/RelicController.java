package com.hack.heroes.travelgo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hack.heroes.travelgo.entity.Relic;
import com.hack.heroes.travelgo.maps.CustomRelicUserMap;
import com.hack.heroes.travelgo.maps.RelicUserMap;
import com.hack.heroes.travelgo.service.RelicService;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(path = "/relic")
public class RelicController {

	// approximate conversion from decimal degrees to meters for Poland
	private final double dd01 = 800.0;
	
	@Autowired
	private RelicService relicService;
	
	@GetMapping(path = "/{latitude}/{longitude}/{radiusInMeters}")
	public ResponseEntity<List<Relic>> getRelicsInRadius(@PathVariable double latitude, @PathVariable double longitude, @PathVariable double radiusInMeters){
		return relicService.getRelicsInRadius(latitude, longitude, (double)(radiusInMeters/dd01) * 0.01);
	}
	
	@GetMapping(path = "/{id}")
	public ResponseEntity<Relic> getRelic(@PathVariable long id){
		return relicService.getRelic(id);
	}
	
	@GetMapping(path = "/all")
	public ResponseEntity<Iterable<Relic>> getRelics(){
		return relicService.getRelics();
	}
	
	@PostMapping(path = "/add/custom")
	public ResponseEntity<String> addCustomRelic(@RequestBody CustomRelicUserMap crum){
		return relicService.addCustomRelic(crum);
	}
	
	@DeleteMapping(path = "/delete/custom")
	public ResponseEntity<String> deleteCustomRelic(@RequestBody RelicUserMap rum){
		return relicService.removeCustomRelic(rum);
	}
	
}
