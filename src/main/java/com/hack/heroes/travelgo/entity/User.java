package com.hack.heroes.travelgo.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User {

	@Id
	@GeneratedValue
	private long id;
	private String username;
	private String password;
	private int level;
	private int currentExperience;
	private int leftExperience;
	
	@ManyToMany
	private List<Relic> relics;
	
	@ManyToMany
	private List<Relic> customRelics;
	
	public List<Relic> getCustomRelics() {
		return customRelics;
	}

	public void setCustomRelics(List<Relic> customRelics) {
		this.customRelics = customRelics;
	}

	public User() {
		level = 1;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public List<Relic> getRelics() {
		return relics;
	}

	public void setRelics(List<Relic> relics) {
		this.relics = relics;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getCurrentExperience() {
		return currentExperience;
	}

	public void setCurrentExperience(int currentExperience) {
		this.currentExperience = currentExperience;
	}

	public int getLeftExperience() {
		return leftExperience;
	}

	public void setLeftExperience(int leftExperience) {
		this.leftExperience = leftExperience;
	}
	
}
