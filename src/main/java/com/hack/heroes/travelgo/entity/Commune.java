package com.hack.heroes.travelgo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Commune {

	@Id
	private int id;
	private String name;
	private int density;
	private double latitude;
	private double longitude;
	
	public Commune() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDensity() {
		return density;
	}

	public void setDensity(int density) {
		this.density = density;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	
}
