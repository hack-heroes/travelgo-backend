package com.hack.heroes.travelgo.service;

import static java.util.Collections.emptyList;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.hack.heroes.travelgo.entity.Relic;
import com.hack.heroes.travelgo.entity.User;
import com.hack.heroes.travelgo.maps.RelicUserMap;
import com.hack.heroes.travelgo.repository.RelicRepository;
import com.hack.heroes.travelgo.repository.UserRepository;
import com.hack.heroes.travelgo.utils.MathUtils;

@Service
public class UserService implements UserDetailsService{

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private RelicRepository relicRepo;
	
	@Autowired
	private MathUtils mathUtils;

	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public UserService(BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	public ResponseEntity<String> addNewUser(User user) {
		Optional<User> optionalUser = userRepo.findByUsername(user.getUsername());
		if (optionalUser.isPresent()) {
			return new ResponseEntity<>("User already exists", HttpStatus.FORBIDDEN);
		} else {
			if(user.getUsername().trim().length() <= 0 || user.getPassword().trim().length() <= 0) {
				return new ResponseEntity<>("Username or password is missing", HttpStatus.NOT_ACCEPTABLE);
			}
			User newUser = new User();
			newUser.setUsername(user.getUsername());
			newUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
			newUser.setLeftExperience(mathUtils.returnExperience(1));
			userRepo.save(newUser);
			return new ResponseEntity<>("User Created", HttpStatus.OK);
		}

	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> user = userRepo.findByUsername(username);
		if (user.get() == null) {
			throw new UsernameNotFoundException(username);
		}
		return new org.springframework.security.core.userdetails.User(user.get().getUsername(),
				user.get().getPassword(), emptyList());
	}

	public ResponseEntity<User> getUser(String username) {
		Optional<User> optionalUser = userRepo.findByUsername(username);
		if (!optionalUser.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(optionalUser.get(), HttpStatus.OK);
		}
	}
	
	public ResponseEntity<String> addExp(String username, int experience){
		Optional<User> optionalUser = userRepo.findByUsername(username);
		if(!optionalUser.isPresent()) {
			return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
		} else {
			if(experience < 0) return new ResponseEntity<>("Experience is lower than 0", HttpStatus.NOT_ACCEPTABLE);
			
			User user = optionalUser.get();
			
			int userExp = user.getCurrentExperience();
			userExp = userExp + experience;
			user.setCurrentExperience(userExp);
			while(user.getCurrentExperience() >= user.getLeftExperience()) {
				int leftExp = user.getLeftExperience();
				user.setLevel(user.getLevel() + 1);
				user.setCurrentExperience(user.getCurrentExperience() - user.getLeftExperience());
				user.setLeftExperience(user.getLeftExperience() + mathUtils.returnExperience(user.getLevel())/2);
				if(user.getLevel() > 40) {
					user.setLevel(40);
					user.setCurrentExperience(leftExp - 1);
					user.setLeftExperience(leftExp);
				}
			}
			
			userRepo.save(user);
			
			return new ResponseEntity<>("Exp added", HttpStatus.OK);
		}
			
	}
	
	public ResponseEntity<List<Relic>> getUserRelic(String username){
		Optional<User> optionalUser = userRepo.findByUsername(username);
		if(!optionalUser.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			User user = optionalUser.get();
			return new ResponseEntity<>(user.getRelics(), HttpStatus.OK);
		}
	}
	
	public ResponseEntity<List<Relic>> getUserCustomRelic(String username){
		Optional<User> optionalUser = userRepo.findByUsername(username);
		if(!optionalUser.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			User user = optionalUser.get();
			return new ResponseEntity<>(user.getCustomRelics(), HttpStatus.OK);
		}
	}

	public ResponseEntity<Iterable<User>> getUsers() {
		Iterable<User> users = userRepo.findAll();
		if (users == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		else
			return new ResponseEntity<>(users, HttpStatus.OK);
	}
	
	public ResponseEntity<User> visitRelic(RelicUserMap rum){
		Optional<User> optionalUser = userRepo.findByUsername(rum.getUsername());
		Optional<Relic> optionalRelic = relicRepo.findById(rum.getRelicId());
		if(!optionalUser.isPresent() || !optionalRelic.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			boolean hasRelic = false;
			Relic relic = optionalRelic.get();
			User user = optionalUser.get();
			for(Relic r : user.getRelics()) {
				if(r.getId() == relic.getId())
					hasRelic = true;
			}
			if(hasRelic) return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
			
			user.getRelics().add(relic);
			
			int userExp = user.getCurrentExperience();
			userExp = userExp + relic.getExp();
			user.setCurrentExperience(userExp);
			while(user.getCurrentExperience() >= user.getLeftExperience()) {
				int leftExp = user.getLeftExperience();
				user.setLevel(user.getLevel() + 1);
				user.setCurrentExperience(user.getCurrentExperience() - user.getLeftExperience());
				user.setLeftExperience(user.getLeftExperience() + mathUtils.returnExperience(user.getLevel())/2);
				if(user.getLevel() > 40) {
					user.setLevel(40);
					user.setCurrentExperience(leftExp - 1);
					user.setLeftExperience(leftExp);
				}
			}
			
			userRepo.save(user);
			
			return new ResponseEntity<>(user, HttpStatus.OK);
		}		
	}
	
}
