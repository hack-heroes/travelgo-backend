package com.hack.heroes.travelgo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.hack.heroes.travelgo.entity.Relic;
import com.hack.heroes.travelgo.entity.User;
import com.hack.heroes.travelgo.maps.CustomRelicUserMap;
import com.hack.heroes.travelgo.maps.RelicUserMap;
import com.hack.heroes.travelgo.repository.CommuneRepository;
import com.hack.heroes.travelgo.repository.RelicRepository;
import com.hack.heroes.travelgo.repository.UserRepository;
import com.hack.heroes.travelgo.utils.MathUtils;

@Service
public class RelicService {

	@Autowired
	private RelicRepository relicRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private CommuneRepository communeRepo;
	
	@Autowired
	private MathUtils mathUtils;
	
	public ResponseEntity<List<Relic>> getRelicsInRadius(double latitude, double longitude, double radius){
		List<Relic> relics = new ArrayList<>();
		relics = relicRepo.relicsInRadius((double)latitude - radius, (double)latitude + radius, (double) longitude - radius, (double) longitude + radius);
		return new ResponseEntity<>(relics, HttpStatus.OK);
	}
	
	public ResponseEntity<Relic> getRelic(long id){
		Optional<Relic> optionalRelic = relicRepo.findById(id);
		if(!optionalRelic.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(optionalRelic.get(), HttpStatus.OK);
		}
	}
	
	public ResponseEntity<Iterable<Relic>> getRelics(){
		Iterable<Relic> relics = relicRepo.findAll();
		if(relics == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(relics, HttpStatus.OK);
		}
	}
	
	public ResponseEntity<String> addCustomRelic(CustomRelicUserMap crum){
		Optional<User> optionalUser = userRepo.findByUsername(crum.getUsername());
		List<Relic> relics = relicRepo.relics();
		if(!optionalUser.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			User user = optionalUser.get();
			Relic relic = new Relic();
			relic.setId(relicRepo.lastRelicId() + 1);
			relic.setIdentification(crum.getIdentification());
			if(crum.getDatingOfObj() != null)
				relic.setDatingOfObj(crum.getDatingOfObj());
			else
				relic.setDatingOfObj("");
			relic.setLatitude(crum.getLatitude());
			relic.setLongitude(crum.getLongitude());
			relic.setCommuneName("");
			relic.setPlaceName("");
			relic.setVoivodeshipName("");
			relic.setDistrictName("");
			relic.setCountryCode("PL");
			if(crum.getRegisterNumber() != null)
				relic.setRegisterNumber(crum.getRegisterNumber());
			else
				relic.setRegisterNumber("");
			Relic nearestRelic = mathUtils.returnNearest(relic, relics);
			relic.setExp(nearestRelic.getExp());
						
			if(user.getCustomRelics().size() >= user.getLevel()) {
				System.out.println(user.getCustomRelics().size());
				System.out.println(user.getLevel());
				return new ResponseEntity<>("limit",HttpStatus.NOT_ACCEPTABLE);
			}
			
			if(mathUtils.returnDistance(relic, nearestRelic) < 0.00100314006) {
				return new ResponseEntity<>("Za blisko", HttpStatus.NOT_ACCEPTABLE);
			}
			
			relicRepo.save(relic);
			user.getCustomRelics().add(relic);
			userRepo.save(user);
			
			return new ResponseEntity<>("Object Added", HttpStatus.OK);
		}		
	}
	
	public ResponseEntity<String> removeCustomRelic(RelicUserMap rum){
		Optional<User> optionalUser = userRepo.findByUsername(rum.getUsername());
		Relic customRelic = null;
		if(!optionalUser.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			User user = optionalUser.get();
			List<Relic> customRelics = user.getCustomRelics();
			for(Relic r : customRelics) {
				if(r.getId() == rum.getRelicId())
					customRelic = r;
			}
			if(customRelic == null) return new ResponseEntity<>("Custom relic doesnt belong to user", HttpStatus.NOT_FOUND);
			user.getCustomRelics().remove(customRelic);
			userRepo.save(user);
			relicRepo.delete(customRelic);
			return new ResponseEntity<>("Relic deleted", HttpStatus.OK);
		}
		
	}
	
	
}
