package com.hack.heroes.travelgo.utils;

import java.util.List;

import com.hack.heroes.travelgo.entity.Commune;
import com.hack.heroes.travelgo.entity.Relic;

public class MathUtils {

	public int returnExperience(int level) {
		int levels = 40;
		int xpForFirstLevel = 1000;
		int xpForLastLevel = 1000000;
		
		double B = (Math.log(xpForLastLevel / xpForFirstLevel) / (levels - 1));
		double A = xpForFirstLevel / (Math.exp(B) - 1);
		
		int oldXp = (int) Math.round(A * Math.exp(B * (level - 1)));
		int newXp = (int) Math.round(A * Math.exp(B * level));
		
		return newXp - oldXp;
	}
	
	public double returnDistance(Relic relic, Commune commune) {
		double latitudeDiff = relic.getLatitude() - commune.getLatitude();
		double longitudeDiff = relic.getLongitude() - commune.getLongitude();
		
		double A = Math.pow(latitudeDiff, 2);
		double B = Math.pow(longitudeDiff, 2);
		double C = A + B;
		
		return Math.sqrt(C);
	}
	
	public double returnDistance(Relic relic, Relic relic2) {
		double latitudeDiff = relic.getLatitude() - relic2.getLatitude();
		double longitudeDiff = relic.getLongitude() - relic2.getLongitude();
		
		double A = Math.pow(latitudeDiff, 2);
		double B = Math.pow(longitudeDiff, 2);
		double C = A + B;
		
		return Math.sqrt(C);
	}
	
	public Relic returnNearest(Relic relic, List<Relic> relics) {
		double closestDistance = 1000;
		Relic closestRelic = null;
		
		for(Relic r : relics) {
			double dist = returnDistance(relic, r);
			if(dist < closestDistance) {
				closestDistance = dist;
				closestRelic = r;
			}
		}
		
		return closestRelic;
	}
		
}
